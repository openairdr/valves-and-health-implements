**Open Air RD**

En Latinoamérica y el Caribe, especialmente en República Dominicana, los centros de salud no cuentan con suficientes Unidades de Cuidados Intensivos (UCI) para pacientes que requieran de respiración asistida, esto es debido al alto costo de estos dispositivos (~USD$30,000.00 por unidad). 

En la actualidad, República Dominicana cuenta solamente con 400 unidades de cuidado intensivo (UCI) con ventilador (incluyendo centros de salud públicos y privados) más 244 nuevas unidades que se han adquirido para un total de 644, para una población de +10 millones de habitantes, según el censo del 2017, y donde las válvulas son de un solo uso.

Todo esto representa una situación de muy alto riesgo para enfrentarnos a la pandemia COVID-19 teniendo en consideración que normalmente la ocupación de estas UCI es de un 90% (por personas que padecen de afecciones respiratorias), y que un 3% de los pacientes con COVID-19 necesita ser asistido por un ventilador, lo que se espera que en las próximas 6 semanas escale entre un 6% - 10%, como se ha visto en el caso de Italia que a pesar de ser un país desarrollado, su infraestructura de salud ha colapsado por no contar con una mayor cantidad de ventiladores, válvulas y otros implementos para este tipo de afecciones. 

La mayor concentración de UCIs y ventiladores en el país se encuentra en Santo Domingo en clínicas privadas, y el resto son repartidos en los 175 hospitales del Ministerio de Salud Pública. En el caso de las camas de aislamiento, el país cuenta con aproximadamente 100 camas repartidas entre el Hospital José María Cabral y Báez, Hospital Militar Ramón de Lara, y nuevas habilitaciones en el Hospital General Docente de la Policía Nacional.



**1. PROBLEMA** 

Nuestro objetivo es proveer al sector salud de varios implementos tanto para el personal médico como para la atención de los pacientes, a partir del uso de materiales y máquinas de impresión 3D, routers CNC, cortadores láser, entre otros, para garantizar la demanda.

Para el personal médico, se crearán caretas protectoras (Protective Shields) que protegen sus ojos y cara de los estornudos y tos de los pacientes, con piezas impresas en 3D y planchas de acetato; para los pacientes se elaborarán, bajo impresión 3D, las válvulas desechables para los CPAPs (Continuous Positive Airway Pressure), que son utilizados por lo general por pacientes ambulatorios que son capaces de exhalar por sí mismos, lo que representaría la liberación de un alto porcentaje de UCI con ventiladores para los pacientes de condiciones más graves; asimismo, fabricar dispositivos que permitan asistir a dos personas con un solo ventilador para aumentar la disponibilidad y disminuir cuellos de botella en salas de emergencia de los centros de salud del país; y por último, fabricar ventiladores automáticos de mucho menor costo para proveer a los hospitales de todo el país con suficientes unidades para enfrentar las crisis.

Todos estos dispositivos se realizarán bajo estrictos protocolos con impresoras estereolitográficas de grado del sector salud para las impresiones, y demás implementos con materiales y partes disponibles en el mercado local; y son de tipo Open Source, lo que permite que pueda ser replicado y extendido a todas las provincias del país.


**2. SOLUCIÓN** 

Nuestro objetivo es proveer al sector salud de varios implementos tanto para el personal médico como para la atención de los pacientes, a partir del uso de materiales y máquinas de impresión 3D, routers CNC, cortadores láser, entre otros, para garantizar la demanda.

Para el personal médico, se crearán caretas protectoras (Protective Shields) que protegen sus ojos y cara de los estornudos y tos de los pacientes, con piezas impresas en 3D y planchas de acetato; para los pacientes se elaborarán, bajo impresión 3D, las válvulas desechables para los CPAPs (Continuous Positive Airway Pressure), que son utilizados por lo general por pacientes ambulatorios que son capaces de exhalar por sí mismos, lo que representaría la liberación de un alto porcentaje de UCI con ventiladores para los pacientes de condiciones más graves; asimismo, fabricar dispositivos que permitan asistir a dos personas con un solo ventilador para aumentar la disponibilidad y disminuir cuellos de botella en salas de emergencia de los centros de salud del país; y por último, fabricar ventiladores automáticos de mucho menor costo para proveer a los hospitales de todo el país con suficientes unidades para enfrentar las crisis.

Todos estos dispositivos se realizarán bajo estrictos protocolos con impresoras estereolitográficas de grado del sector salud para las impresiones, y demás implementos con materiales y partes disponibles en el mercado local; y son de tipo Open Source, lo que permite que pueda ser replicado y extendido a todas las provincias del país.
 

**3. BENEFICIARIO**

Nuestra misión es beneficiar a todos los pacientes de República Dominicana, Latinoamérica y el Caribe con situaciones respiratorias agudas, independientemente de su edad o estrato social, que visitan las salas de emergencia de los centros de salud, al garantizar alta disponibilidad del servicio, sobre todo en situaciones de emergencia.

También velar por disminuir la exposición de los profesionales del sector salud al proveerles implementos que les permitan realizar sus labores.

En una primera fase nuestro objetivo es aportar 100 ventiladores y 10,000 caretas protectoras.



